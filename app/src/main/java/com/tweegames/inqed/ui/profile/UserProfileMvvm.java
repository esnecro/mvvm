package com.tweegames.inqed.ui.profile;

import android.databinding.ObservableBoolean;
import android.view.View;

import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.viewmodel.AdapterMvvmViewModel;

public interface UserProfileMvvm {

    interface UsersView extends MvvmView {
        void onRefresh(boolean success);
    }

    interface ViewModel extends AdapterMvvmViewModel<UsersView> {
        void loadProfile();
        void loadPosts();
        void loadMorePosts();

        void onReloadButtonClick(View v);

        ObservableBoolean isLoad();
        ObservableBoolean isCanNotLoad();
    }
}
