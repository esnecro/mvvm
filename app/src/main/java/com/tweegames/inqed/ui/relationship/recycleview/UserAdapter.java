package com.tweegames.inqed.ui.relationship.recycleview;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tweegames.inqed.R;
import com.tweegames.inqed.data.model.entities.User;
import com.tweegames.inqed.injection.scopes.PerFragment;
import com.tweegames.inqed.ui.relationship.recycleview.item.UserViewHolder;
import com.tweegames.inqed.utils.Utils;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

@PerFragment
public class UserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<User> countryList = Collections.emptyList();

    @Inject
    public UserAdapter() { }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return Utils.createViewHolder(viewGroup, R.layout.item_list_follow, UserViewHolder::new);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        try {
            ((UserViewHolder) viewHolder).viewModel().update(countryList.get(position));
            ((UserViewHolder) viewHolder).executePendingBindings();
        } catch (Exception e) {
                e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

    public void setCountryList(List<User> countryList) {
        if(this.countryList.isEmpty())
            this.countryList = countryList;
        else
            this.countryList.addAll(countryList);

        notifyItemRangeInserted(this.countryList.size(), countryList.size());
    }

}
