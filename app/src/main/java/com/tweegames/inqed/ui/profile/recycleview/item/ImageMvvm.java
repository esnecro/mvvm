package com.tweegames.inqed.ui.profile.recycleview.item;

import com.tweegames.inqed.data.model.entities.Image;
import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.viewmodel.MvvmViewModel;

public interface ImageMvvm {

    interface ViewModel extends MvvmViewModel<MvvmView> {

        void update(Image image);

        Image getImage();

        String getUrl();
    }
}
