package com.tweegames.inqed.ui.profile.recycleview.item;

import android.view.View;

import com.tweegames.inqed.databinding.ItemGridProfileBinding;
import com.tweegames.inqed.ui.base.BaseViewHolder;
import com.tweegames.inqed.ui.base.view.MvvmView;

public class ImageViewHolder extends BaseViewHolder<ItemGridProfileBinding, ImageMvvm.ViewModel> implements MvvmView {

    public ImageViewHolder(View v) {
        super(v);

        viewHolderComponent().inject(this);
        bindContentView(v);
    }
}
