package com.tweegames.inqed.ui.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tweegames.inqed.BR;
import com.tweegames.inqed.injection.components.DaggerViewHolderComponent;
import com.tweegames.inqed.injection.components.ViewHolderComponent;
import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.viewmodel.MvvmViewModel;
import com.tweegames.inqed.ui.base.viewmodel.NoOpViewModel;
import com.tweegames.inqed.utils.Utils;

import javax.inject.Inject;

public abstract class BaseViewHolder<B extends ViewDataBinding, V extends MvvmViewModel> extends RecyclerView.ViewHolder {

    protected B binding;

    @Inject
    protected V viewModel;

    protected final View itemView;

    private ViewHolderComponent viewHolderComponent;

    public BaseViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    protected final ViewHolderComponent viewHolderComponent() {
        if(viewHolderComponent == null) {
            viewHolderComponent = DaggerViewHolderComponent.builder()
                    .activityComponent(Utils.castActivityFromContext(itemView.getContext(), BaseActivity.class).activityComponent())
                    .build();
        }

        return viewHolderComponent;
    }

    protected final void bindContentView(@NonNull View view) {
        if(viewModel == null) { throw new IllegalStateException("viewModel must not be null and should be injected via viewHolderComponent().inject(this)"); }
        binding = DataBindingUtil.bind(view);
        binding.setVariable(BR.vm, viewModel);

        try {
            //noinspection unchecked
            viewModel.attachView((MvvmView) this, null);
        } catch(ClassCastException e) {
            if (!(viewModel instanceof NoOpViewModel)) {
                throw new RuntimeException(getClass().getSimpleName() + " must implement MvvmView subclass as declared in " + viewModel.getClass().getSimpleName());
            }
        }
    }

    public final V viewModel() {
        return viewModel;
    }

    public final void executePendingBindings() {
        if(binding != null) { binding.executePendingBindings(); }
    }
}
