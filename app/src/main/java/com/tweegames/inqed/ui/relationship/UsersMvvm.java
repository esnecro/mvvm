package com.tweegames.inqed.ui.relationship;

import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.viewmodel.AdapterMvvmViewModel;

public interface UsersMvvm {

    interface UsersView extends MvvmView {
        void onRefresh(boolean success);
    }

    interface ViewModel extends AdapterMvvmViewModel<UsersView> {

        void initKind(String kind);
        void loadData();
        void loadMore();
    }
}
