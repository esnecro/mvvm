package com.tweegames.inqed.ui.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tweegames.inqed.BR;
import com.tweegames.inqed.injection.components.DaggerFragmentComponent;
import com.tweegames.inqed.injection.components.FragmentComponent;
import com.tweegames.inqed.injection.modules.FragmentModule;
import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.viewmodel.MvvmViewModel;
import com.tweegames.inqed.ui.base.viewmodel.NoOpViewModel;

import javax.inject.Inject;

public abstract class BaseFragment <B extends ViewDataBinding, V extends MvvmViewModel> extends Fragment {

    protected B binding;

    @Inject
    protected V viewModel;

    private FragmentComponent mFragmentComponent;

    protected  ActionBar actionBar;

    protected String title;

    protected final FragmentComponent fragmentComponent() {
        if(mFragmentComponent == null) {
            mFragmentComponent = DaggerFragmentComponent.builder()
                    .fragmentModule(new FragmentModule(this))
                    .activityComponent(((BaseActivity) getActivity()).activityComponent())
                    .build();
        }

        return mFragmentComponent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    protected final View setAndBindContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, @LayoutRes int layoutResID) {
        if(viewModel == null) { throw new IllegalStateException("viewModel must already be set via injection"); }
        binding = DataBindingUtil.inflate(inflater, layoutResID, container, false);
        binding.setVariable(BR.vm, viewModel);

        try {

            viewModel.attachView((MvvmView) this, savedInstanceState);
        } catch(ClassCastException e) {
            if (!(viewModel instanceof NoOpViewModel)) {
                throw new RuntimeException(getClass().getSimpleName() + " must implement MvvmView subclass as declared in " + viewModel.getClass().getSimpleName());
            }
        }

        return binding.getRoot();
    }

    @Override
    @CallSuper
    public void onDestroyView() {
        super.onDestroyView();
        if(viewModel != null) {
            viewModel.detachView();
        }
        binding = null;
        /*viewModel = null;*/
    }

    @Override
    @CallSuper
    public void onDestroy() {
        super.onDestroy();

        mFragmentComponent = null;
    }

}

