package com.tweegames.inqed.ui.relationship.recycleview.item;

import android.view.View;

import com.tweegames.inqed.databinding.ItemListFollowBinding;
import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.BaseViewHolder;

public class UserViewHolder extends BaseViewHolder<ItemListFollowBinding, UserMvvm.ViewModel> implements MvvmView {

    public UserViewHolder(View v) {
        super(v);

        viewHolderComponent().inject(this);
        bindContentView(v);
    }
}
