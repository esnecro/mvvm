package com.tweegames.inqed.ui.profile.recycleview.header;

import android.view.View;

import com.tweegames.inqed.databinding.HeaderGridProfileBinding;
import com.tweegames.inqed.ui.base.BaseViewHolder;
import com.tweegames.inqed.ui.base.view.MvvmView;

public class HeaderProfileViewHolder extends BaseViewHolder<HeaderGridProfileBinding, HeaderProfileMvvm.ViewModel> implements MvvmView {

    public HeaderProfileViewHolder(View v) {
        super(v);

        viewHolderComponent().inject(this);
        bindContentView(v);
    }
}
