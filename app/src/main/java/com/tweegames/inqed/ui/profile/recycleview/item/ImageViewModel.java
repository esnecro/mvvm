package com.tweegames.inqed.ui.profile.recycleview.item;

import android.content.Context;

import com.tweegames.inqed.data.model.entities.Image;
import com.tweegames.inqed.injection.qualifier.AppContext;
import com.tweegames.inqed.injection.scopes.PerViewHolder;
import com.tweegames.inqed.ui.base.navigator.Navigator;
import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.viewmodel.BaseViewModel;

import javax.inject.Inject;

@PerViewHolder
public class ImageViewModel extends BaseViewModel<MvvmView> implements ImageMvvm.ViewModel {

    protected final Context ctx;
    protected final Navigator navigator;

    protected Image image;

    @Inject
    public ImageViewModel(@AppContext Context context, Navigator navigator) {
        this.ctx = context.getApplicationContext();
        this.navigator = navigator;
    }

    @Override
    public void update(Image image) {
        this.image = image;
        notifyChange();
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public String getUrl()   {
        return image.getImages().getFullRes().getUrl();
    }

}