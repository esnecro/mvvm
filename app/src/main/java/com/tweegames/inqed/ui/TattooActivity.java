package com.tweegames.inqed.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.tweegames.inqed.R;
import com.tweegames.inqed.databinding.ActivityTattooBinding;
import com.tweegames.inqed.ui.base.BaseActivity;
import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.viewmodel.NoOpViewModel;
import com.tweegames.inqed.ui.profile.UserProfileFragment;

import java.util.Collections;
import java.util.List;

public class TattooActivity extends BaseActivity<ActivityTattooBinding, NoOpViewModel> implements MvvmView {

    public static final String TAG_PROFILE = "profile";
    public static final String TAG_USER_LIST = "user_list";

    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityComponent().inject(this);
        setAndBindContentView(savedInstanceState, R.layout.activity_tattoo);

        setSupportActionBar(binding.toolbar);

        fragmentManager = getSupportFragmentManager();

        openProfileFragment();
    }

    private void openProfileFragment() {
        UserProfileFragment userProfileFragment = new UserProfileFragment();
        addFragment(userProfileFragment, TAG_PROFILE);
    }

    private void addFragment(Fragment fragment, String tag) {
        fragmentManager.beginTransaction()
                .add(R.id.content_frame, fragment, tag)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @SuppressWarnings("RestrictedApi")
    @Override
    public void onBackPressed() {

        List<Fragment> fragments = fragmentManager.getFragments();
        fragments.removeAll(Collections.singleton(null));
        if (fragments.size() > 1) {
            fragmentManager.beginTransaction()
                    .remove(fragmentManager.findFragmentByTag(TAG_USER_LIST))
                    .commitNow();
        } else {
            Log.d("Test", "OnBackPressed");
            super.onBackPressed();
        }
    }

}