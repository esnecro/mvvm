package com.tweegames.inqed.ui.base.viewmodel;

import android.databinding.BaseObservable;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.tweegames.inqed.ui.base.MvvmViewNotAttachedException;
import com.tweegames.inqed.ui.base.view.MvvmView;

public abstract class BaseViewModel<V extends MvvmView> extends BaseObservable implements MvvmViewModel<V> {

    private V mvvmView;

    @Override
    @CallSuper
    public void attachView(V mvvmView, @Nullable Bundle savedInstanceState) {
        this.mvvmView = mvvmView;
        if(savedInstanceState != null) { restoreInstanceState(savedInstanceState); }
    }

    @Override
    @CallSuper
    public void detachView() {
        mvvmView = null;
    }

    protected void restoreInstanceState(@NonNull Bundle savedInstanceState) { }

    public void saveInstanceState(Bundle outState) { }

    public final boolean isViewAttached() {
        return mvvmView != null;
    }

    public final V getView() {
        return mvvmView;
    }

    public final void checkViewAttached() {
        if (!isViewAttached()) throw new MvvmViewNotAttachedException();
    }
}
