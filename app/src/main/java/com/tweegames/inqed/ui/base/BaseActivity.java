package com.tweegames.inqed.ui.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;

import com.tweegames.inqed.BR;
import com.tweegames.inqed.app.InqedApp;
import com.tweegames.inqed.injection.components.ActivityComponent;
import com.tweegames.inqed.injection.components.DaggerActivityComponent;
import com.tweegames.inqed.injection.modules.ActivityModule;
import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.viewmodel.MvvmViewModel;
import com.tweegames.inqed.ui.base.viewmodel.NoOpViewModel;

import javax.inject.Inject;

public abstract class BaseActivity<B extends ViewDataBinding, V extends MvvmViewModel> extends AppCompatActivity {


    // Inject a Realm instance into every Activity, since the instance
    // is cached and reused for a thread (avoids create/destroy overhead)
    //@Inject protected Realm realm;

    protected B binding;
    @Inject
    protected V viewModel;

    /*@Inject
    RefWatcher refWatcher;*/

    private ActivityComponent mActivityComponent;

    /*@Override
    @CallSuper
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(viewModel != null) { viewModel.saveInstanceState(outState); }
    }*/

    @Override
    @CallSuper
    protected void onDestroy() {
        super.onDestroy();
        /*if(refWatcher != null) {
            refWatcher.watch(mActivityComponent);
            if(viewModel != null) { refWatcher.watch(viewModel); }
        }*/
        if(viewModel != null) { viewModel.detachView(); }
        binding = null;
        viewModel = null;
        mActivityComponent = null;
        //if(realm != null) { realm.close(); }
    }

    protected final ActivityComponent activityComponent() {
        if (mActivityComponent == null) {
            mActivityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .appComponent(InqedApp.getAppComponent())
                    .build();
        }
        return mActivityComponent;

    }

    /* Sets the content view, creates the binding and attaches the view to the view model */
    protected final void setAndBindContentView(@Nullable Bundle savedInstanceState, @LayoutRes int layoutResID) {
        if(viewModel == null) { throw new IllegalStateException("viewModel must already be set via injection"); }
        binding = DataBindingUtil.setContentView(this, layoutResID);
        binding.setVariable(BR.vm, viewModel);

        try {
            //noinspection unchecked
            viewModel.attachView((MvvmView) this, savedInstanceState);
        } catch(ClassCastException e) {
            if (!(viewModel instanceof NoOpViewModel)) {
                throw new RuntimeException(getClass().getSimpleName() + " must implement MvvmView subclass as declared in " + viewModel.getClass().getSimpleName());
            }
        }
    }

    public int dimen(@DimenRes int resId) {
        return (int) getResources().getDimension(resId);
    }

    public int color(@ColorRes int resId) {
        return getResources().getColor(resId);
    }

    public int integer(@IntegerRes int resId) {
        return getResources().getInteger(resId);
    }

    public String string(@StringRes int resId) {
        return getResources().getString(resId);
    }
}
