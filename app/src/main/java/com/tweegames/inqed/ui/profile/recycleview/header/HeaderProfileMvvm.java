package com.tweegames.inqed.ui.profile.recycleview.header;

import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.tweegames.inqed.data.model.entities.ProfileUser;
import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.viewmodel.MvvmViewModel;

public interface HeaderProfileMvvm {

    interface ViewModel extends MvvmViewModel<MvvmView> {

        void onFollowsButtonClick(View v);
        void onFollowersButtonClick(View v);
        void onPostsClick(View v);
        void update(ProfileUser user);

        ProfileUser getUser();

        String getUsername();
        String getFullname();
        String getLocation();
        String getProfilePicture();
        String getRating();

        String getFollowsCount();
        String getFollowersCount();
        String getPostsCount();

        ObservableField<Drawable> getProfileImage();
    }
}
