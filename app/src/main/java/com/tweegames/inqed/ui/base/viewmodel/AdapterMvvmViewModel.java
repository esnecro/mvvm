package com.tweegames.inqed.ui.base.viewmodel;

import android.databinding.ObservableBoolean;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tweegames.inqed.ui.base.view.MvvmView;

public interface AdapterMvvmViewModel<V extends MvvmView> extends MvvmViewModel<V> {

    RecyclerView.Adapter getAdapter();

    void onLoadMoreButtonClick(View v);

    ObservableBoolean isLoadMore();
    ObservableBoolean isCanNotLoadMore();
}
