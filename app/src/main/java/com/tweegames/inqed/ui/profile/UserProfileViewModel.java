package com.tweegames.inqed.ui.profile;

import android.databinding.ObservableBoolean;
import android.view.View;

import com.tweegames.inqed.data.model.entities.Image;
import com.tweegames.inqed.data.model.entities.ProfileUser;
import com.tweegames.inqed.data.model.responses.EntityResponse;
import com.tweegames.inqed.data.model.responses.ListResponse;
import com.tweegames.inqed.data.remote.ApiInterface;
import com.tweegames.inqed.injection.scopes.PerFragment;
import com.tweegames.inqed.ui.base.viewmodel.BaseViewModel;
import com.tweegames.inqed.ui.profile.recycleview.ImageAdapter;
import com.tweegames.inqed.utils.Constants;
import com.tweegames.inqed.utils.QueryParams;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

@PerFragment
public class UserProfileViewModel extends BaseViewModel<UserProfileMvvm.UsersView> implements UserProfileMvvm.ViewModel {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private ImageAdapter adapter;
    private ApiInterface apiInterface;

    private EntityResponse<ProfileUser> userResponse = new EntityResponse<>();
    private ListResponse<Image> postsResponse = new ListResponse<>();

    private final ObservableBoolean load = new ObservableBoolean(false);
    private final ObservableBoolean canNotLoad = new ObservableBoolean(false);

    private final ObservableBoolean loadMore = new ObservableBoolean(false);
    private final ObservableBoolean canNotLoadMore = new ObservableBoolean(false);

    @Inject
    public UserProfileViewModel(ImageAdapter adapter, ApiInterface apiInterface) {
        this.adapter = adapter;
        this.apiInterface = apiInterface;
    }

    @Override
    public void detachView() {
        super.detachView();
        compositeDisposable.clear();
    }

    @Override
    public void loadProfile() {
        load.set(true);

        QueryParams params = getProfileParams();
        compositeDisposable.add(apiInterface.getUser(params.getHeaders(), params.getPath())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    userResponse.setEntity(user.getEntity());
                    userResponse.setMeta(user.getMeta());

                    adapter.setUser(user.getEntity());
                    load.set(false);
                    loadPosts();
                }, throwable ->  {
                    getView().onRefresh(false);
                    load.set(false);
                    canNotLoad.set(true);
                }));
    }

    @Override
    public void loadPosts() {

        QueryParams params = getPostsParams();
        compositeDisposable.add(apiInterface.getPosts(params.getHeaders(), params.getPath(), params.getParams())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(posts -> {
                    postsResponse.setList(posts.getList());
                    postsResponse.setMeta(posts.getMeta());

                    adapter.setPostsList(posts.getList());
                    adapter.notifyDataSetChanged();
                    getView().onRefresh(true);
                    loadMore.set(false);
                }, throwable ->  {
                    getView().onRefresh(false);
                    loadMore.set(false);
                    canNotLoadMore.set(true);
                }));

    }

    @Override
    public void loadMorePosts() {
        if(postsResponse.getList().size() < postsResponse.getMeta().getMetaTotal()) {
            loadMore.set(true);
            loadPosts();
        }
    }

    private QueryParams getProfileParams()    {

        String path = "users/" + Constants.USER_ID;
        QueryParams params = new QueryParams(Constants.TOKEN, path, null);
        return params;
    }

    private QueryParams getPostsParams()    {

        String path = "users/" + Constants.USER_ID + "/posts";
        HashMap<String, String> hm = new HashMap<>();

        if(postsResponse.getList().size() > 0)
            hm.put("before" , String.valueOf(postsResponse.getMeta().getMetaBefore()));
        hm.put("count", Constants.LOAD_COUNT);

        QueryParams params = new QueryParams(Constants.TOKEN, path, hm);

        return params;
    }

    @Override
    public ImageAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void onReloadButtonClick(View v) {
        canNotLoad.set(false);
        loadProfile();
    }

    @Override
    public void onLoadMoreButtonClick(View v) {
        canNotLoadMore.set(false);
        loadMorePosts();
    }

    @Override
    public ObservableBoolean isLoad() {
        return load;
    }

    @Override
    public ObservableBoolean isLoadMore() {
        return loadMore;
    }

    @Override
    public ObservableBoolean isCanNotLoad() {
        return canNotLoad;
    }

    @Override
    public ObservableBoolean isCanNotLoadMore() {
        return canNotLoadMore;
    }

}
