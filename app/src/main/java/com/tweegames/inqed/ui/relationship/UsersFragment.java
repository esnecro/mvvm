package com.tweegames.inqed.ui.relationship;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tweegames.inqed.R;
import com.tweegames.inqed.databinding.FragmentFollowBinding;
import com.tweegames.inqed.ui.base.BaseFragment;
import com.tweegames.inqed.ui.relationship.recycleview.UserAdapter;
import com.tweegames.inqed.utils.Utils;
import com.tweegames.inqed.utils.scrolllistener.RecyclerOnScrollListener;
import com.tweegames.inqed.utils.scrolllistener.init.CountsDefault;

import javax.inject.Inject;

public class UsersFragment extends BaseFragment<FragmentFollowBinding, UsersMvvm.ViewModel> implements UsersMvvm.UsersView {

    @Inject
    UserAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return setAndBindContentView(inflater, container, savedInstanceState, R.layout.fragment_follow);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

        binding.rvMain.setHasFixedSize(true);
        binding.rvMain.setLayoutManager(layoutManager);
        binding.rvMain.setAdapter(adapter);
        binding.rvMain.addOnScrollListener(new RecyclerOnScrollListener(layoutManager, new CountsDefault()) {
            @Override
            public void onLoadMore() {
                viewModel.loadData();
            }
        });

        viewModel.initKind(getArguments().getString("kind"));
        viewModel.loadData();
    }

    @Override
    public void onRefresh(boolean success) {

        if(!success) {
            Utils.showToast(getContext(), R.string.some_error);
        }
    }
}
