package com.tweegames.inqed.ui.base.viewmodel;

import android.databinding.Observable;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.tweegames.inqed.ui.base.view.MvvmView;

public interface MvvmViewModel<V extends MvvmView> extends Observable {
    void attachView(V view, Bundle savedInstanceState);
    void detachView();

    void saveInstanceState(@NonNull Bundle outState);
}
