package com.tweegames.inqed.ui.relationship.recycleview.item;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatDrawableManager;
import android.view.View;

import com.tweegames.inqed.BR;
import com.tweegames.inqed.R;
import com.tweegames.inqed.data.model.entities.User;
import com.tweegames.inqed.data.remote.ApiInterface;
import com.tweegames.inqed.injection.qualifier.AppContext;
import com.tweegames.inqed.injection.scopes.PerViewHolder;
import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.viewmodel.BaseViewModel;
import com.tweegames.inqed.utils.Constants;
import com.tweegames.inqed.utils.QueryParams;
import com.tweegames.inqed.utils.Utils;
import com.tweegames.inqed.utils.bindingadapter.BindableFieldTarget;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

@PerViewHolder
public class UserViewModel extends BaseViewModel<MvvmView> implements UserMvvm.ViewModel {

    private Context ctx;

    private User user;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ApiInterface apiInterface;

    private ObservableBoolean loaded = new ObservableBoolean(true);
    private ObservableField<Drawable> profileImage = new ObservableField<>();

    private BindableFieldTarget bindableFieldTarget;

    @Inject
    public UserViewModel(@AppContext Context context, ApiInterface apiInterface) {
        this.ctx = context.getApplicationContext();
        this.apiInterface = apiInterface;

        bindableFieldTarget = new BindableFieldTarget(profileImage, context.getResources());
    }

    @Override
    public void detachView() {
        super.detachView();
        compositeDisposable.clear();
    }

    @Override
    public void onRelationshipButtonClick(View v) {
        loaded.set(false);

        QueryParams params = getFollowActionParams();

        Single d;
        if(user.getFollowed() == 0)
            d = apiInterface.postAction(params.getHeaders(), params.getPath());
        else
            d = apiInterface.deleteAction(params.getHeaders(), params.getPath());

        compositeDisposable.add(d
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(meta -> {
                    loaded.set(true);
                    user.changeRelationship();
                    //notifyChange();
                    notifyPropertyChanged(BR.relationshipDrawable);
                }, throwable ->  {
                    loaded.set(true);
                }));
    }

    private QueryParams getFollowActionParams()    {

        String path = Utils.makeRelationshipActionPath(user.getUserId());
        return new QueryParams(Constants.TOKEN, path, null);
    }

    @Override
    public void update(User user) {
        this.user = user;
        Utils.showProfilePicture(ctx, user.getProfilePicture(), bindableFieldTarget);
        notifyChange();
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public String getFullname() {
        return user.getFullName();
    }

    @Override
    public String getProfilePicture()   {
        return user.getProfilePicture();
    }

    @Override
    public boolean isMaster()    {
        return user.getRole().equals(Constants.MASTER);
    }

    @Override
    //@Bindable
    @SuppressWarnings("RestrictedApi")
    public Drawable getRelationshipDrawable() {
        return AppCompatDrawableManager.get().getDrawable(ctx, user.getFollowed() == 1 ? R.drawable.ic_follow : R.drawable.ic_unfollow);
        //return ctx.getResources().getDrawable(user.getFollowed() == 0 ? R.drawable.ic_follow : R.drawable.ic_unfollow);
    }

    @Override
    public ObservableBoolean isLoaded() {
        return loaded;
    }

    @Override
    public ObservableField<Drawable> getProfileImage() {
        return profileImage;
    }

    @Override
    public String getRating()   {
        return String.valueOf(user.getRating());
    }

}