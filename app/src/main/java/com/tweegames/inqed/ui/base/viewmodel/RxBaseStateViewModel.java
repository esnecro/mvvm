package com.tweegames.inqed.ui.base.viewmodel;

import com.tweegames.inqed.ui.base.view.MvvmView;

import io.reactivex.disposables.CompositeDisposable;

public abstract class RxBaseStateViewModel<T extends MvvmView, S> extends BaseStateViewModel<T, S> {

    protected final CompositeDisposable disposable = new CompositeDisposable();

    @Override
    public void detachView() {
        super.detachView();
        disposable.clear();
    }
}
