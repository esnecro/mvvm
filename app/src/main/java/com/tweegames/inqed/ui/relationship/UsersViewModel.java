package com.tweegames.inqed.ui.relationship;

import android.databinding.ObservableBoolean;
import android.view.View;

import com.tweegames.inqed.data.model.entities.User;
import com.tweegames.inqed.data.model.responses.ListResponse;
import com.tweegames.inqed.data.remote.ApiInterface;
import com.tweegames.inqed.injection.scopes.PerFragment;
import com.tweegames.inqed.utils.QueryParams;
import com.tweegames.inqed.ui.relationship.recycleview.UserAdapter;
import com.tweegames.inqed.ui.base.viewmodel.BaseViewModel;
import com.tweegames.inqed.utils.Constants;
import com.tweegames.inqed.utils.Utils;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

@PerFragment
public class UsersViewModel extends BaseViewModel<UsersMvvm.UsersView> implements UsersMvvm.ViewModel {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private UserAdapter adapter;
    private ApiInterface apiInterface;

    private ListResponse<User> usersResponse = new ListResponse<>();

    private final ObservableBoolean loadMore = new ObservableBoolean(false);
    private final ObservableBoolean canNotLoadMore = new ObservableBoolean(false);

    private String kind;

    @Inject
    public UsersViewModel(UserAdapter adapter, ApiInterface apiInterface) {
        this.adapter = adapter;
        this.apiInterface = apiInterface;
    }

    @Override
    public void detachView() {
        super.detachView();
        compositeDisposable.clear();
    }

    @Override
    public void initKind(String kind) {
        this.kind = kind;
    }

    @Override
    public void loadData() {
        loadMore.set(true);

        QueryParams params = getFollowListParams();
        compositeDisposable.add(apiInterface.getUsers(params.getHeaders(), params.getPath(), params.getParams())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(users -> {
                    usersResponse.setList(users.getList());
                    usersResponse.setMeta(users.getMeta());

                    adapter.setCountryList(users.getList());
                    adapter.notifyDataSetChanged();
                    getView().onRefresh(true);
                    loadMore.set(false);
                }, throwable ->  {
                    getView().onRefresh(false);
                    loadMore.set(false);
                    canNotLoadMore.set(true);
                }));
    }

    @Override
    public void loadMore() {
        if(usersResponse.getList().size() < usersResponse.getMeta().getMetaTotal()) {
            loadData();
        }

    }

    private QueryParams getFollowListParams()    {

        String path = Utils.makeGetFollowsPath(Constants.USER_ID, kind);
        HashMap<String, String> hm = new HashMap<>();

        if(!usersResponse.getList().isEmpty())
            hm.put("before" , String.valueOf(usersResponse.getMeta().getMetaBefore()));
        hm.put("count", Constants.LOAD_COUNT);

        QueryParams params = new QueryParams(Constants.TOKEN, path, hm);
        return params;
    }

    @Override
    public UserAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void onLoadMoreButtonClick(View v) {
        canNotLoadMore.set(false);
        loadData();

    }

    @Override
    public ObservableBoolean isLoadMore() {
        return loadMore;
    }

    @Override
    public ObservableBoolean isCanNotLoadMore() {
        return canNotLoadMore;
    }

}
