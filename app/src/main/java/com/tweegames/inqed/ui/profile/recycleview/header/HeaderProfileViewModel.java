package com.tweegames.inqed.ui.profile.recycleview.header;

import android.content.Context;
import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import com.tweegames.inqed.R;
import com.tweegames.inqed.data.model.entities.ProfileUser;
import com.tweegames.inqed.injection.qualifier.AppContext;
import com.tweegames.inqed.injection.scopes.PerViewHolder;
import com.tweegames.inqed.ui.TattooActivity;
import com.tweegames.inqed.ui.base.navigator.Navigator;
import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.viewmodel.BaseViewModel;
import com.tweegames.inqed.ui.relationship.UsersFragment;
import com.tweegames.inqed.utils.Constants;
import com.tweegames.inqed.utils.Utils;
import com.tweegames.inqed.utils.bindingadapter.BindableFieldTarget;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

@PerViewHolder
public class HeaderProfileViewModel extends BaseViewModel<MvvmView> implements HeaderProfileMvvm.ViewModel {

    private Context ctx;
    private Navigator navigator;

    private ProfileUser user;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private ObservableField<Drawable> profileImage = new ObservableField<>();

    private BindableFieldTarget bindableFieldTarget;

    @Inject
    public HeaderProfileViewModel(@AppContext Context context, Navigator navigator) {
        this.ctx = context.getApplicationContext();
        this.navigator = navigator;

        bindableFieldTarget = new BindableFieldTarget(profileImage, context.getResources());
    }

    @Override
    public void detachView() {
        super.detachView();
        compositeDisposable.clear();
    }

    @Override
    public void onFollowsButtonClick(View v) {
        Bundle args = new Bundle();
        args.putString("kind", Constants.FOLLOWS);
        //navigator.replaceFragmentAndAddToBackStack(R.id.content_frame, new UsersFragment(), args, TattooActivity.TAG_USER_LIST);
        navigator.addFragment(R.id.content_frame, new UsersFragment(), TattooActivity.TAG_USER_LIST, args);
    }

    @Override
    public void onFollowersButtonClick(View v) {
        Bundle args = new Bundle();
        args.putString("kind", Constants.FOLLOWERS);
        navigator.addFragment(R.id.content_frame, new UsersFragment(), TattooActivity.TAG_USER_LIST, args);
    }

    @Override
    public void onPostsClick(View v) {

    }

    @Override
    public void update(ProfileUser user) {
        this.user = user;
        Utils.showProfilePicture(ctx, user.getProfilePicture(), bindableFieldTarget);
        notifyChange();
    }

    @Override
    public ProfileUser getUser() {
        return user;
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public String getFullname() {
        return user.getFullName();
    }

    @Override
    public String getLocation() {
        return user.getLocation().getLoc_name();
    }

    @Override
    public String getProfilePicture() {
        return user.getProfilePicture();
    }

    @Override
    public String getRating() {
        return String.valueOf(user.getRating());
    }

    @Override
    public String getFollowsCount() {
        return String.valueOf(user.getCounts().getFollows());
    }

    @Override
    public String getFollowersCount() {
        return String.valueOf(user.getCounts().getFollowers());
    }

    @Override
    public String getPostsCount() {
        return String.valueOf(user.getCounts().getPosts());
    }

    @Override
    public ObservableField<Drawable> getProfileImage() {
        return profileImage;
    }

}
