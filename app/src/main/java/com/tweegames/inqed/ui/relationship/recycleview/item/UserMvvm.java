package com.tweegames.inqed.ui.relationship.recycleview.item;

import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.tweegames.inqed.data.model.entities.User;
import com.tweegames.inqed.ui.base.view.MvvmView;
import com.tweegames.inqed.ui.base.viewmodel.MvvmViewModel;

public interface UserMvvm {

    interface ViewModel extends MvvmViewModel<MvvmView> {

        void onRelationshipButtonClick(View v);
        void update(User user);

        // Properties
        User getUser();

        String getUsername();
        String getFullname();
        String getProfilePicture();

        String getRating();
        boolean isMaster();

        @Bindable
        Drawable getRelationshipDrawable();

        ObservableBoolean isLoaded();
        ObservableField<Drawable> getProfileImage();
    }
}
