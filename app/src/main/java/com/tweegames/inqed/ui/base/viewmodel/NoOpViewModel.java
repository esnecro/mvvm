package com.tweegames.inqed.ui.base.viewmodel;

import android.databinding.BaseObservable;
import android.os.Bundle;

import com.tweegames.inqed.ui.base.view.MvvmView;

import javax.inject.Inject;

public final class NoOpViewModel extends BaseObservable implements MvvmViewModel<MvvmView> {

    @Inject
    public NoOpViewModel() { }

    @Override
    public void attachView(MvvmView mvvmView, Bundle savedInstanceState) { }

    @Override
    public void saveInstanceState(Bundle outState) { }

    @Override
    public void detachView() { }

}
