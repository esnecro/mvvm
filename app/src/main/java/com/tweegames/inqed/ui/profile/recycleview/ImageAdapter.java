package com.tweegames.inqed.ui.profile.recycleview;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tweegames.inqed.R;
import com.tweegames.inqed.data.model.entities.Image;
import com.tweegames.inqed.data.model.entities.ProfileUser;
import com.tweegames.inqed.injection.scopes.PerFragment;
import com.tweegames.inqed.ui.profile.recycleview.header.HeaderProfileViewHolder;
import com.tweegames.inqed.ui.profile.recycleview.item.ImageViewHolder;
import com.tweegames.inqed.utils.Utils;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

@PerFragment
public class ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_REGULAR = 1;

    private ProfileUser user;
    private List<Image> countryList = Collections.emptyList();

    @Inject
    public ImageAdapter() { }

    @Override
    public int getItemViewType(int position) {
        if(position == 0)
            return TYPE_HEADER;
        else
            return TYPE_REGULAR;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        if (viewType == TYPE_HEADER) {
            return Utils.createViewHolder(viewGroup, R.layout.header_grid_profile, HeaderProfileViewHolder::new);

        } else {
            return Utils.createViewHolder(viewGroup, R.layout.item_grid_profile, ImageViewHolder::new);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        try {
            if (viewHolder instanceof ImageViewHolder) {
                ((ImageViewHolder) viewHolder).viewModel().update(countryList.get(position - 1));
                ((ImageViewHolder) viewHolder).executePendingBindings();
            }   else    {
                ((HeaderProfileViewHolder) viewHolder).viewModel().update(user);
                ((HeaderProfileViewHolder) viewHolder).executePendingBindings();
            }
        }catch (Exception e) {
                e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (countryList.size() == 0)
            return 0;
        return countryList.size() + 1;
    }

    public void setUser(ProfileUser user) {
        this.user = user;
        notifyItemRangeInserted(0, 1);
    }

    public void setPostsList(List<Image> countryList) {
        if(this.countryList.isEmpty())
            this.countryList = countryList;
        else
            this.countryList.addAll(countryList);

        notifyItemRangeInserted(this.countryList.size() + 1, countryList.size());
    }

}
