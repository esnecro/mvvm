package com.tweegames.inqed.ui.base.viewmodel;

import android.databinding.BaseObservable;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.tweegames.inqed.ui.base.MvvmViewNotAttachedException;
import com.tweegames.inqed.ui.base.view.MvvmView;

import javax.inject.Inject;

public abstract class BaseStateViewModel<T extends MvvmView, S> extends BaseObservable implements MvvmViewModel<T> {

    private final String KEY_STATE = "state";

    private T mvvmView;
    @Inject
    protected S state;

    @Override
    @CallSuper
    public void attachView(T mvpView, @Nullable Bundle savedInstanceState) {
        mvvmView = mvpView;
        if(savedInstanceState != null) { restoreInstanceState(savedInstanceState); }
    }

    @Override
    @CallSuper
    public void detachView() {
        mvvmView = null;
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        //outState.putParcelable(KEY_STATE, Parcels.wrap(state));
    }

    protected void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        if(savedInstanceState.containsKey(KEY_STATE)) {
            //state = Parcels.unwrap(savedInstanceState.getParcelable(KEY_STATE));
        }
    }

    public final boolean isViewAttached() {
        return mvvmView != null;
    }

    public final T getMvvmView() {
        return mvvmView;
    }

    public final void checkViewAttached() {
        if (!isViewAttached()) throw new MvvmViewNotAttachedException();
    }

    public S getState() {
        return state;
    }
}
