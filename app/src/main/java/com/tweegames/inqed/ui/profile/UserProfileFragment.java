package com.tweegames.inqed.ui.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tweegames.inqed.R;
import com.tweegames.inqed.databinding.FragmentProfileBinding;
import com.tweegames.inqed.ui.base.BaseFragment;
import com.tweegames.inqed.ui.profile.recycleview.ImageAdapter;
import com.tweegames.inqed.utils.Utils;
import com.tweegames.inqed.utils.scrolllistener.RecyclerOnScrollListener;
import com.tweegames.inqed.utils.scrolllistener.init.CountsDefault;
import com.tweegames.inqed.utils.scrolllistener.init.CountsWithHeader;

import javax.inject.Inject;

public class UserProfileFragment extends BaseFragment<FragmentProfileBinding, UserProfileMvvm.ViewModel> implements UserProfileMvvm.UsersView {

    @Inject
    ImageAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return setAndBindContentView(inflater, container, savedInstanceState, R.layout.fragment_profile);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override public int getSpanSize(int position) {
                return (position == 0) ? 3 : 1;
            }
        });

        binding.rvMain.setHasFixedSize(true);
        binding.rvMain.setLayoutManager(layoutManager);
        binding.rvMain.setAdapter(adapter);
        binding.rvMain.addOnScrollListener(new RecyclerOnScrollListener(layoutManager, new CountsWithHeader()) {
            @Override
            public void onLoadMore() {
                viewModel.loadMorePosts();
            }
        });

        viewModel.loadProfile();
    }

    @Override
    public void onRefresh(boolean success) {

        if(!success) {
            Utils.showToast(getContext(), R.string.some_error);
        }
    }
}
