package com.tweegames.inqed.injection.components;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.tweegames.inqed.injection.modules.ActivityModule;
import com.tweegames.inqed.injection.modules.ViewModelModule;
import com.tweegames.inqed.injection.qualifier.ActivityContext;
import com.tweegames.inqed.injection.qualifier.ActivityFragmentManager;
import com.tweegames.inqed.injection.scopes.PerActivity;
import com.tweegames.inqed.ui.TattooActivity;
import com.tweegames.inqed.ui.base.navigator.Navigator;

import dagger.Component;

@PerActivity
@Component(dependencies = AppComponent.class, modules = {ActivityModule.class, ViewModelModule.class})
public interface ActivityComponent extends AppComponent {

    @ActivityContext
    Context activityContext();

    @ActivityFragmentManager
    FragmentManager defaultFragmentManager();

    Navigator navigator();

    // create inject methods for your Activities here
    void inject(TattooActivity activity);

}
