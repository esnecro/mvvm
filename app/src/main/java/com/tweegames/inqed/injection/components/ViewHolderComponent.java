package com.tweegames.inqed.injection.components;

import com.tweegames.inqed.injection.modules.ViewHolderModule;
import com.tweegames.inqed.injection.modules.ViewModelModule;
import com.tweegames.inqed.injection.scopes.PerViewHolder;
import com.tweegames.inqed.ui.relationship.recycleview.item.UserViewHolder;
import com.tweegames.inqed.ui.profile.recycleview.header.HeaderProfileViewHolder;
import com.tweegames.inqed.ui.profile.recycleview.item.ImageViewHolder;

import dagger.Component;

@PerViewHolder
@Component(dependencies = ActivityComponent.class, modules = {ViewHolderModule.class, ViewModelModule.class})
public interface ViewHolderComponent {
    void inject(UserViewHolder viewHolder);
    void inject(ImageViewHolder viewHolder);

    void inject(HeaderProfileViewHolder viewHolder);
}
