package com.tweegames.inqed.injection.components;

import com.tweegames.inqed.injection.modules.FragmentModule;
import com.tweegames.inqed.injection.modules.ViewModelModule;
import com.tweegames.inqed.injection.scopes.PerFragment;
import com.tweegames.inqed.ui.relationship.UsersFragment;
import com.tweegames.inqed.ui.profile.UserProfileFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = ActivityComponent.class, modules = {FragmentModule.class, ViewModelModule.class})
public interface FragmentComponent {
    void inject(UsersFragment fragment);
    void inject(UserProfileFragment fragment);
}
