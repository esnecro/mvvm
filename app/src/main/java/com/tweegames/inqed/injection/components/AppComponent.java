package com.tweegames.inqed.injection.components;

import android.content.Context;
import android.content.res.Resources;

import com.tweegames.inqed.data.remote.ApiInterface;
import com.tweegames.inqed.injection.modules.AppModule;
import com.tweegames.inqed.injection.modules.NetModule;
import com.tweegames.inqed.injection.qualifier.AppContext;
import com.tweegames.inqed.injection.scopes.PerApplication;

import dagger.Component;

@PerApplication
@Component(modules={AppModule.class, NetModule.class/*, DataModule.class*/})
public interface AppComponent {
    @AppContext
    Context appContext();
    Resources resources();

    ApiInterface countryApi();
}