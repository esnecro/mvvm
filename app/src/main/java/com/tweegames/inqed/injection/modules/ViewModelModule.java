package com.tweegames.inqed.injection.modules;

import com.tweegames.inqed.ui.relationship.UsersMvvm;
import com.tweegames.inqed.ui.relationship.UsersViewModel;
import com.tweegames.inqed.ui.relationship.recycleview.item.UserMvvm;
import com.tweegames.inqed.ui.relationship.recycleview.item.UserViewModel;
import com.tweegames.inqed.ui.profile.UserProfileMvvm;
import com.tweegames.inqed.ui.profile.UserProfileViewModel;
import com.tweegames.inqed.ui.profile.recycleview.header.HeaderProfileMvvm;
import com.tweegames.inqed.ui.profile.recycleview.header.HeaderProfileViewModel;
import com.tweegames.inqed.ui.profile.recycleview.item.ImageMvvm;
import com.tweegames.inqed.ui.profile.recycleview.item.ImageViewModel;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelModule {

    // Activities


    // Fragments

    @Binds
    abstract UsersMvvm.ViewModel bindUsersViewModel(UsersViewModel usersViewModel);

    @Binds
    abstract UserProfileMvvm.ViewModel bindUserProfileViewModel(UserProfileViewModel usersViewModel);


    // View Holders

    @Binds
    abstract UserMvvm.ViewModel bindUserViewModel(UserViewModel userViewModel);

    @Binds
    abstract ImageMvvm.ViewModel bindImageViewModel(ImageViewModel imageViewModel);

    @Binds
    abstract HeaderProfileMvvm.ViewModel bindHeaderProfileViewModel(HeaderProfileViewModel profileViewModel);

}
