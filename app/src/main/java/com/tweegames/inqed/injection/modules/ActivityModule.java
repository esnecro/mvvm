package com.tweegames.inqed.injection.modules;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.tweegames.inqed.injection.qualifier.ActivityContext;
import com.tweegames.inqed.injection.qualifier.ActivityFragmentManager;
import com.tweegames.inqed.injection.scopes.PerActivity;
import com.tweegames.inqed.ui.base.navigator.ActivityNavigator;
import com.tweegames.inqed.ui.base.navigator.Navigator;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private final AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        mActivity = activity;
    }

    @Provides
    @PerActivity
    @ActivityContext
    Context provideActivityContext() { return mActivity; }

    @Provides
    @PerActivity
    @ActivityFragmentManager
    FragmentManager provideFragmentManager() { return mActivity.getSupportFragmentManager(); }

    @Provides
    @PerActivity
    Navigator provideNavigator() { return new ActivityNavigator(mActivity); }

}
