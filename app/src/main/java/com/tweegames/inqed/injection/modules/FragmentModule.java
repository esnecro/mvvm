package com.tweegames.inqed.injection.modules;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.tweegames.inqed.injection.qualifier.ChildFragmentManager;
import com.tweegames.inqed.injection.scopes.PerFragment;
import com.tweegames.inqed.ui.base.navigator.ChildFragmentNavigator;
import com.tweegames.inqed.ui.base.navigator.FragmentNavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentModule {

    private final Fragment mFragment;

    public FragmentModule(Fragment fragment) {
        mFragment = fragment;
    }

    @Provides
    @PerFragment
    @ChildFragmentManager
    FragmentManager provideChildFragmentManager() { return mFragment.getChildFragmentManager(); }

    @Provides
    @PerFragment
    FragmentNavigator provideFragmentNavigator() { return new ChildFragmentNavigator(mFragment); }
}
