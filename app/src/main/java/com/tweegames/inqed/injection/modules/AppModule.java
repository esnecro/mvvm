package com.tweegames.inqed.injection.modules;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.tweegames.inqed.injection.qualifier.AppContext;
import com.tweegames.inqed.injection.scopes.PerApplication;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final Application mApp;

    public AppModule(Application app) {
        mApp = app;
    }

    @Provides
    @PerApplication
    @AppContext
    Context provideAppContext() {
        return mApp;
    }

    @Provides
    @PerApplication
    Resources provideResources() {
        return mApp.getResources();
    }

}
