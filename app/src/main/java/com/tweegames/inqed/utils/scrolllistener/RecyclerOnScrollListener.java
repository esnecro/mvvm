package com.tweegames.inqed.utils.scrolllistener;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tweegames.inqed.utils.scrolllistener.init.ICounts;

public abstract class RecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    protected int mPreviousTotal = 0;
    protected boolean mLoading = true;

    protected LinearLayoutManager mLinearLayoutManager;

    protected ICounts counts;

    protected RecyclerOnScrollListener(LinearLayoutManager linearLayoutManager, ICounts counts) {
        this.mLinearLayoutManager = linearLayoutManager;
        this.counts = counts;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        counts.init(recyclerView.getChildCount(),
                    mLinearLayoutManager.getItemCount(),
                    mLinearLayoutManager.findFirstVisibleItemPosition());

        /*int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = mLinearLayoutManager.getItemCount();
        int firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();*/

        if (mLoading) {
            if (counts.getTotalCounts() > mPreviousTotal) {
                mLoading = false;
                mPreviousTotal = counts.getTotalCounts();
            }
        }
        int visibleThreshold = 0;
        if (!mLoading && (counts.getTotalCounts() - counts.getVisibleCounts())
                <= (counts.getFirstItem() + visibleThreshold)) {

            onLoadMore();

            mLoading = true;
        }
    }

    public abstract void onLoadMore();
}
