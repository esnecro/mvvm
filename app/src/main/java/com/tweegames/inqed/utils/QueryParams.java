package com.tweegames.inqed.utils;

import com.tweegames.inqed.utils.Utils;

import java.util.HashMap;
import java.util.List;

/**
 * Данный класс используется для формирования всех параметров запроса, генерируется подпись, помещаются необходимые хэдеры, указывается путь.
 */
public class QueryParams {

    private String path;
    private HashMap<String, String> headers = new HashMap<>();
    private HashMap<String, String> params;

    public QueryParams(String token, String path, HashMap<String, String> params)    {
        initHeaders(token, path, params);

        this.params = params;
        this.path = path;
    }

    public void initHeaders(String token, String path, HashMap<String, String> params) {
        String sign;
        if(params != null)
            sign = Utils.getSign(path, params);
        else
            sign = path;

        if(token != null)
            headers.put("X-Request-Token", token);

        headers.put("X-Request-Sign", Utils.hmacDigest(sign));
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(HashMap<String, String> headers) {
        this.headers = headers;
    }

    public HashMap<String, String> getParams() {
        return params;
    }

    public void setParams(HashMap<String, String> params) {
        this.params = params;
    }

}
