package com.tweegames.inqed.utils.scrolllistener.init;

public interface ICounts {

    public void init(int visibleCounts, int totalCounts, int firstItem);
    public int getVisibleCounts();
    public int getTotalCounts();
    public int getFirstItem();
}
