package com.tweegames.inqed.utils.bindingadapter;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.tweegames.inqed.utils.Utils;

public class BindingAdapters {

    @BindingAdapter("android:visibility")
    public static void setVisibility(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("bind:imageUrl")
    public static void loadImage(ImageView view, String url) {
        Utils.showPicture(view.getContext(), url, view);
    }

    /*@BindingAdapter("android:src")
    public static void loadProfileImage(ImageView view, String url) {
        Utils.showProfilePicture(view.getContext(), url, view);
    }*/
}
