package com.tweegames.inqed.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tweegames.inqed.R;
import com.tweegames.inqed.utils.bindingadapter.BindableFieldTarget;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class Utils {

    private static final String HASH_ALGORITHM = "HmacSHA256";

    public static String makeGetFollowsPath(int id, String kind)  {
        return "users/" + id + "/" + kind;
    }

    public static String makeRelationshipActionPath(int id)  {
        return "users/" + id + "/relationship";
    }

    public static void showToast(Context context, int resId)    {
        Toast toast = Toast.makeText(context,
                context.getResources().getString(resId), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    public static String hmacDigest(String text) {

        String secretKey = "1";

        try {
            Key sk = new SecretKeySpec(secretKey.getBytes(), HASH_ALGORITHM);
            Mac mac = Mac.getInstance(sk.getAlgorithm());
            mac.init(sk);
            final byte[] hmac = mac.doFinal(text.getBytes());
            return toHexString(hmac);
        } catch (NoSuchAlgorithmException e1) {

        } catch (InvalidKeyException e) {

        }

        return null;
    }

    public static String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);

        Formatter formatter = new Formatter(sb);
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }

        return sb.toString();
    }

    /**
     * Формируем подпись запроса для дальнейшего шифрования и помещения в header,
     * все параметры должны быть отсортированы в алфавитном порядке.
     */
    public static String getSign(String method, HashMap<String, String> map)    {

        Map<String, String> treeMap = new TreeMap<>(map);
        String sign = method;

        for(Map.Entry<String, String> entry : treeMap.entrySet()) {
            sign = sign + ";" + entry.getKey() + "=>" + entry.getValue();
        }

        return sign;
    }

    public static void showPicture(Context context, String url, ImageView image) {

        Picasso.with(context)
                .load(url)
                .fit()
                .placeholder(R.color.color_place_holder)
                .noFade()
                .centerCrop()
                .into(image);
    }

    public static void showProfilePicture(Context context, String url, ImageView image) {

        Picasso.with(context)
                .load(url)
                .error(R.drawable.ic_profile_preview)
                .noFade()
                //.placeholder(R.drawable.ic_profile_preview)
                .transform(new CircleTransform())
                .into(image);
    }

    public static void showProfilePicture(Context context, String url, BindableFieldTarget target) {

        Picasso.with(context)
                .load(url)
                .error(R.drawable.ic_profile_preview)
                .noFade()
                //.placeholder(R.drawable.ic_profile_preview)
                .transform(new CircleTransform())
                .into(target);
    }

    // Usage: createViewHolder(viewGroup, R.layout.my_layout, MyViewHolder::new);
    public static <T extends RecyclerView.ViewHolder> T createViewHolder(@NonNull ViewGroup viewGroup, @LayoutRes int layoutResId, @NonNull PlainFunction<View, T> newViewHolderAction) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(layoutResId, viewGroup, false);

        return newViewHolderAction.apply(view);
    }

    // Tries to cast an Activity Context to another type
    @SuppressWarnings("unchecked")
    @Nullable
    public static <T> T castActivityFromContext(Context context, Class<T> castClass) {
        if(castClass.isInstance(context)) {
            return (T) context;
        }

        while(context instanceof ContextWrapper) {
            context = ((ContextWrapper) context).getBaseContext();

            if(castClass.isInstance(context)) {
                return (T) context;
            }
        }

        return null;
    }

}