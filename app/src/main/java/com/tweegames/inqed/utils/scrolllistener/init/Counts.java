package com.tweegames.inqed.utils.scrolllistener.init;

public abstract class Counts {

    protected int visibleCounts;
    protected int totalCounts;
    protected int firstItem;

    public int getVisibleCounts() {
        return visibleCounts;
    }

    public void setVisibleCounts(int visibleCounts) {
        this.visibleCounts = visibleCounts;
    }

    public int getTotalCounts() {
        return totalCounts;
    }

    public void setTotalCounts(int totalCounts) {
        this.totalCounts = totalCounts;
    }

    public int getFirstItem() {
        return firstItem;
    }

    public void setFirstItem(int firstItem) {
        this.firstItem = firstItem;
    }
}
