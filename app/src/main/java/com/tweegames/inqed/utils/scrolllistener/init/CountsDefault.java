package com.tweegames.inqed.utils.scrolllistener.init;

public class CountsDefault extends Counts implements ICounts {

    @Override
    public void init(int visibleCounts, int totalCounts, int firstItem) {
        this.visibleCounts = visibleCounts;
        this.totalCounts = totalCounts;
        this.firstItem = firstItem;

    }
}
