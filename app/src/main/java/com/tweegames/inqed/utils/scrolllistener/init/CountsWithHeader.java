package com.tweegames.inqed.utils.scrolllistener.init;

public class CountsWithHeader extends Counts implements ICounts {

    @Override
    public void init(int visibleCounts, int totalCounts, int firstItem) {
        this.visibleCounts = visibleCounts - 1;
        this.totalCounts = totalCounts - 1;
        this.firstItem = firstItem + 1;

    }
}
