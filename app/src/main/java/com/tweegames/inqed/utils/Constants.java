package com.tweegames.inqed.utils;

public class Constants {

    public static final String API_VERSION = "1";

    public static final String BASE_URL = "https://api.inqed.me/";

    public static final String TOKEN = "Ykh7ApzsYNeSoJLUmWsWFKGz"; //наш токен

    public static final int MY_USER_ID = 146; //наш id
    public static final int USER_ID = 3; //id пользователя, чьи подписки мы будем просматривать

    public static final String MASTER = "master";

    public static final String FOLLOWS = "follows";
    public static final String FOLLOWERS = "followers";

    public static final String LOAD_COUNT = "20";

}