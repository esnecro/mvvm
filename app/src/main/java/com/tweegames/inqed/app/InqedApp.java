package com.tweegames.inqed.app;

import android.content.res.Resources;
import android.support.multidex.MultiDexApplication;

import com.tweegames.inqed.injection.components.AppComponent;
import com.tweegames.inqed.injection.components.DaggerAppComponent;
import com.tweegames.inqed.injection.modules.AppModule;

public class InqedApp extends MultiDexApplication {

    private static InqedApp instance = null;
    private static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        component = buildComponent();
    }

    protected AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static InqedApp getInstance() { return instance; }

    public static AppComponent getAppComponent() {
        return component;
    }

    public static Resources getRes() { return instance.getResources(); }

}
