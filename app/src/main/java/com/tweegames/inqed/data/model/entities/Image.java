package com.tweegames.inqed.data.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Image {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("caption")
    @Expose
    private String caption = "";

    @SerializedName("created_at")
    @Expose
    private long created_at;

    @SerializedName("liked")
    @Expose
    private int liked;

    @SerializedName("images")
    private Images images;

    @SerializedName("from")
    private User from;

    @SerializedName("counts")
    private Counts counts;

    public class Images {

        @SerializedName("thumb")
        @Expose
        private ImageType thumb;

        @SerializedName("low_resolution")
        @Expose
        private ImageType lowRes;

        @SerializedName("full_resolution")
        @Expose
        private ImageType fullRes;

        public class ImageType   {

            @SerializedName("url")
            @Expose
            private String url;

            @SerializedName("width")
            @Expose
            private int width;

            @SerializedName("height")
            @Expose
            private int height;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public int getWidth() {
                return width;
            }

            public void setWidth(int width) {
                this.width = width;
            }

            public int getHeight() {
                return height;
            }

            public void setHeight(int height) {
                this.height = height;
            }

        }

        public ImageType getThumb() {
            return thumb;
        }

        public void setThumb(ImageType thumb) {
            this.thumb = thumb;
        }

        public ImageType getLowRes() {
            return lowRes;
        }

        public void setLowRes(ImageType lowRes) {
            this.lowRes = lowRes;
        }

        public ImageType getFullRes() {
            return fullRes;
        }

        public void setFullRes(ImageType fullRes) {
            this.fullRes = fullRes;
        }
    }

    public class Counts {

        @SerializedName("likes")
        @Expose
        private int count_likes;

        @SerializedName("comments")
        @Expose
        private int count_comments;

        public int getCount_likes() {
            return count_likes;
        }

        public void setCount_likes(int count_likes) {
            this.count_likes = count_likes;
        }

        public int getCount_comments() {
            return count_comments;
        }

        public void setCount_comments(int count_comments) {
            this.count_comments = count_comments;
        }
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public int getLiked() {
        return liked;
    }

    public void setLiked(int liked) {
        this.liked = liked;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public Counts getCounts() {
        return counts;
    }

    public void setCounts(Counts counts) {
        this.counts = counts;
    }

}