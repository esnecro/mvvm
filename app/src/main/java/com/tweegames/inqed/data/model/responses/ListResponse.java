package com.tweegames.inqed.data.model.responses;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс для получения списка сущностей от сервера (список подписчиков, изображений и т.д.)
 * Тип сущности указывается при создании объекта класса
 */
public class ListResponse<T> extends MetaResponse  {

    @SerializedName("data")
    @Expose
    private List<T> list = new ArrayList<>();

    @Nullable
    public List<T> getList() {
        return list;
    }

    @Nullable
    public void setList(List<T> list)  {
        this.list.addAll(list);
    }

}