package com.tweegames.inqed.data.model.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Класс для получения от сервера конкретной сущности (user, comment, post)
 * Тип сущности указывается при создании объекта класса.
 */
public class EntityResponse<T> extends MetaResponse {

    @SerializedName("data")
    @Expose
    private T entity;

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }
}
