package com.tweegames.inqed.data.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Style {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("masters")
    @Expose
    public int masters_count;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMasters_count() {
        return masters_count;
    }

    public void setMasters_count(int masters_count) {
        this.masters_count = masters_count;
    }
}