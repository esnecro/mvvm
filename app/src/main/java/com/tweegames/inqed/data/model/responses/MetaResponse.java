package com.tweegames.inqed.data.model.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tweegames.inqed.data.model.entities.Meta;

/**
 * Ответ от сервера приходит в формате json в виде {"meta":{...},"data":{...}}
 * В классе Meta содержатся различные поля, такие как код выполнения запроса, общее количество элементов в data.
 * От данного класса MetaResponse наследуются EntityResponse и ListResponse (data можем быть или одной сущностью или списком)
 */
public class MetaResponse {

    @SerializedName("meta")
    @Expose
    private Meta meta = new Meta();

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

}
