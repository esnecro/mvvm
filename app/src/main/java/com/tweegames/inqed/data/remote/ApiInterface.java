package com.tweegames.inqed.data.remote;

import com.tweegames.inqed.data.model.entities.Image;
import com.tweegames.inqed.data.model.entities.ProfileUser;
import com.tweegames.inqed.data.model.entities.User;
import com.tweegames.inqed.data.model.responses.EntityResponse;
import com.tweegames.inqed.data.model.responses.ListResponse;
import com.tweegames.inqed.data.model.responses.MetaResponse;

import java.util.HashMap;

import io.reactivex.Single;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface ApiInterface {

    @GET
    Single<EntityResponse<ProfileUser>> getUser(@HeaderMap HashMap<String, String> headers,
                                                @Url String path);

    @GET
    Single<ListResponse<Image>> getPosts(@HeaderMap HashMap<String, String> headers,
                                             @Url String path,
                                             @QueryMap HashMap<String, String> params);

    @GET
    Single<ListResponse<User>> getUsers(@HeaderMap HashMap<String, String> headers,
                                            @Url String path,
                                            @QueryMap HashMap<String, String> params);

    @POST
    Single<MetaResponse> postAction(@HeaderMap HashMap<String, String> headers,
                                    @Url String path);

    @DELETE
    Single<MetaResponse> deleteAction(@HeaderMap HashMap<String, String> headers,
                                          @Url String path);
}
