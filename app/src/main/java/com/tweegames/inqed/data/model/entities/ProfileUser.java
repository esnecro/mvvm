package com.tweegames.inqed.data.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileUser extends User {

    @SerializedName("private")
    @Expose
    public Private priv;

    @SerializedName("counts")
    @Expose
    private Counts counts;

    @SerializedName("location")
    @Expose
    private Location location;

    @SerializedName("confirmation")
    @Expose
    private Confirmation confirm;

    @SerializedName("bio")
    @Expose
    private String bio = null;

    @SerializedName("auth_type")
    @Expose
    private String authType = null;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("registered_at")
    @Expose
    private long registered_at;

    @SerializedName("blacklisted_for_message_notifications")
    @Expose
    private int blocked;

    @SerializedName("subscribed_to_posts_notifications")
    @Expose
    private int subscrided;

    @SerializedName("styles")
    @Expose
    private List<Style> styles;

    public class Private  {

        @SerializedName("email")
        @Expose
        private String email = null;
        @SerializedName("phone_number")
        @Expose
        private String phoneNumber = null;

        @SerializedName("token")
        @Expose
        private String token;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }


        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

    }

    public class Counts     {

        @SerializedName("posts")
        @Expose
        private int posts;
        @SerializedName("follows")
        @Expose
        private int follows;
        @SerializedName("followers")
        @Expose
        private int followers;
        @SerializedName("recieved_reviews")
        @Expose
        private int received_reviews;
        @SerializedName("posted_reviews")
        @Expose
        private int posted_reviews;

        public int getPosts() {
            return posts;
        }

        public void setPosts(int posts) {
            this.posts = posts;
        }

        public int getFollows() {
            return follows;
        }

        public void setFollows(int follows) {
            this.follows = follows;
        }

        public int getFollowers() {
            return followers;
        }

        public void setFollowers(int followers) {
            this.followers = followers;
        }

        public int getReceived_reviews() {
            return received_reviews;
        }

        public void setReceived_reviews(int received_reviews) {
            this.received_reviews = received_reviews;
        }

        public int getPosted_reviews() {
            return posted_reviews;
        }

        public void setPosted_reviews(int posted_reviews) {
            this.posted_reviews = posted_reviews;
        }

        public synchronized void incrementPosts(){
            posts++;
        }

        public synchronized void decrementPosts(){
            posts--;
        }

        public synchronized void incrementFollows(){
            follows++;
        }

        public synchronized void decrementFollows(){
            follows--;
        }

        public synchronized void incrementFollowers(){
            followers++;
        }

        public synchronized void decrementFollowers(){
            followers--;
        }
    }

    public class Location   {

        @SerializedName("name")
        @Expose
        private String loc_name = "";
        @SerializedName("latitude")
        @Expose
        private float loc_lat;    //latitude
        @SerializedName("longitude")
        @Expose
        private float loc_long;    //longitude;

        public String getLoc_name() {
            return loc_name;
        }

        public void setLoc_name(String loc_name) {
            this.loc_name = loc_name;
        }

        public float getLoc_lat() {
            return loc_lat;
        }

        public void setLoc_lat(float loc_lat) {
            this.loc_lat = loc_lat;
        }

        public float getLoc_long() {
            return loc_long;
        }

        public void setLoc_long(float loc_long) {
            this.loc_long = loc_long;
        }
    }

    public class Confirmation   {

        @SerializedName("email")
        @Expose
        private int email_conf;
        @SerializedName("phone_number")
        @Expose
        private int phone_conf;

        public int getEmail_conf() {
            return email_conf;
        }

        public void setEmail_conf(int email_conf) {
            this.email_conf = email_conf;
        }

        public int getPhone_conf() {
            return phone_conf;
        }

        public void setPhone_conf(int phone_conf) {
            this.phone_conf = phone_conf;
        }
    }

    public Private getPriv() {
        return priv;
    }

    public void setPriv(Private priv) {
        this.priv = priv;
    }

    public Counts getCounts() {
        return counts;
    }

    public void setCounts(Counts counts) {
        this.counts = counts;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Confirmation getConfirm() {
        return confirm;
    }

    public void setConfirm(Confirmation confirm) {
        this.confirm = confirm;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getRegistered_at() {
        return registered_at;
    }

    public void setRegistered_at(long registered_at) {
        this.registered_at = registered_at;
    }

    public int getBlocked() {
        return blocked;
    }

    public void setBlocked(int blocked) {
        this.blocked = blocked;
    }

    public int getSubscrided() {
        return subscrided;
    }

    public void setSubscrided(int subscrided) {
        this.subscrided = subscrided;
    }

    public List<Style> getStyles() {
        return styles;
    }

    public void setStyles(List<Style> styles) {
        this.styles.addAll(styles);
    }

}
