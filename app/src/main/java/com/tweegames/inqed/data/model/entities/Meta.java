package com.tweegames.inqed.data.model.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Класс содержит поля, которые могут быть необходимы при дальнейшей работы с полученными данными.
 * Например, чтобы подгрузить следующую страницу списка, используем meta_before для получения текущего последнего загруженного элемента
 * и подружаем новую страницу с элементами, которые находятся после указанного.
 */
public class Meta {

    @SerializedName("next_before_id")
    @Expose
    private int metaBefore;

    @SerializedName("code")
    @Expose
    private int metaStatusCode;

    @SerializedName("total")
    @Expose
    private int metaTotal;

    public int getMetaBefore() {
        return metaBefore;
    }

    public void setMetaBefore(int metaBefore) {
        this.metaBefore = metaBefore;
    }

    public int getMetaStatusCode() {
        return metaStatusCode;
    }

    public void setMetaStatusCode(int metaStatusCode) {
        this.metaStatusCode = metaStatusCode;
    }

    public int getMetaTotal() {
        return metaTotal;
    }

    public void setMetaTotal(int metaTotal) {
        this.metaTotal = metaTotal;
    }

    public synchronized void incrementTotal(){
        metaTotal++;
    }

    public synchronized void decrementTotal(){
        metaTotal--;
    }
}
