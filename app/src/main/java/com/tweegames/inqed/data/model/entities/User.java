package com.tweegames.inqed.data.model.entities;

import android.databinding.Bindable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tweegames.inqed.utils.Constants;

/**
 * В inqed на данный момент существует две глобальные сущности user: Мастер и Клиент;
 * За эту информацию отвечает поле role.
 */
public class User {

    @SerializedName("id")
    @Expose
    private int userId;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("full_name")
    @Expose
    private String fullName;

    @SerializedName("role")
    @Expose
    private String role;

    @SerializedName("profile_picture")
    @Expose
    private String profilePicture;

    @SerializedName("followed")
    @Expose
    private int followed;

    @SerializedName("rating")
    @Expose
    private float rating;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public int getFollowed() {
        return followed;
    }

    public void setFollowed(int followed) {
        this.followed = followed;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public void changeRelationship()    {
        if(followed == 1)
            followed = 0;
        else
            followed = 1;
    }

}
